﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salon
{
    class Transmission
    {
        public string Name;
        public int Id;
        public int Price;

        public Transmission(string Name, int Id, int Price)
        {
            this.Name = Name;
            this.Id = Id;
            this.Price = Price;
        }
    }
}
