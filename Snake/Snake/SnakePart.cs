﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Snake
{
    public class SnakePart : GameObject
    {
        private Texture2D texture;
        private int Speed=20;
        public SnakePart(int x, int y, int width, int height, Texture2D texture)
        {
            this.x = x;
            this.y = y;
            this.Width = width;
            this.Height = height;
            this.texture = texture;
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Rectangle(x, y, 64, 64), Color.White);
        }
        public override void MoveLeft()
        {
            x -= Speed;
        }
        public override void MoveRight()
        {
            x += Speed;
        }
        public override void MoveUp()
        {
            y -= Speed;
        }
        public override void MoveDown()
        {
            y += Speed;
        }
    }
}
