﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pracadomowa28._12._2017
{
    class Kolo : Shape, ICalculatesurface
    {
        public float Obliczpole(float height, float width)
        {
            return 3.14f * height * width;
        }
    }
}
