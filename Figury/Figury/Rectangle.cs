﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury
{
    class Rectangle : IShape
    {
        private float Area;
        private float Circum;
        private List<int> sides = new List<int>();

        public Rectangle()
        {
            RandomizeShape();
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCircum()
        {
            return Circum;
        }

        public IShape RandomizeShape(Random rand)
        {
            sides = new List<int>();
            
            do
            {
                for (int i = 0; i < 2; i++)
                {
                    sides.Add(rand.Next(1, 100));
                }
            } while (!Validate());

            return this;
        }

        public float SetArea()
        {
            int a = sides[0];
            int b = sides[1];


            Area = a * b;

            return Area;
        }

        public float SetCircum()
        {
            int a = sides[0];
            int b = sides[1];

            return Circum = 2 * a * b;
        }

        public bool Validate()
        {
            return true;
                
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCircum();
        

        }

        public IShape RandomizeShape()
        {
            sides = new List<int>();
            Random rand = new Random();
            do
            {
                for (int i = 0; i < 2; i++)
                {
                    sides.Add(rand.Next(1, 100));
                    
                }
            } while (!Validate());

            return this;
        }
    }
}
