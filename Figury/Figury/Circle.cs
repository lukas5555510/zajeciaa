﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury
{
    class Circle : IShape
    {
        int r;
        private float Area;
        private float Circum;
        public Circle()
        {
            RandomizeShape();
        }

        public Circle(Random random)
        {
            RandomizeShape(random);
        } 
        public float GetArea()
        {
            return Area;
        }

        public float GetCircum()
        {
            return Circum;
        }

        public IShape RandomizeShape(Random rand)
        {
            r = (rand.Next(1, 100));
            return this;
        }

        public float SetArea()
        {
            
            Area = (float)(r * r * Math.PI);
            return Area;
        }

        public float SetCircum()
        {
            Circum = (float)(2 * Math.PI * r);
            return Circum;
        }

        public bool Validate()
        {
            return (true);
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCircum();


        }

        public IShape RandomizeShape()
        {
         
            Random rand = new Random();
         r = rand.Next(1, 100);
          

            return this;
        }
    }
}
