﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShape> lista = new List<IShape>();
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                lista.Add(new Triangle());
            }
            for (int i = 0; i < 5; i++)
            {
                lista.Add(new Rectangle());
            }
            for (int i = 0; i < 5; i++)
            {
                lista.Add(new Circle(rand));
            }
            foreach (var item in lista)
            {
                item.SetArea();
                item.SetCircum();
                
            }
           lista = lista.OrderByDescending(x => x.GetArea()).ToList();

            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
        }
    }
}
