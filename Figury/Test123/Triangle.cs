﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test123
{
    class Triangle :IShape
    {
        private float Area;
        private float Circum;
        private List<int> sides = new List<int>();
        public Triangle()
        {
            RandomizeShape();
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCircum()
        {
            return Circum;
        }

        public IShape RandomizeShape()
        {

            Random rand = new Random();
            do
            {
                sides = new List<int>();
                for (int i = 0; i < 3; i++)
                {
                    sides.Add(rand.Next(1, 100));
                }
            } while (Validate());

            return this;
        }

        public IShape RandomizeShape(Random rand)
        {
            sides = new List<int>();

            do
            {
                for (int i = 0; i < 3; i++)
                {
                    sides.Add(rand.Next(1, 100));
                }
            } while (Validate());

            return this;
        }

        public float SetArea()
        {
            sides.Sort();
            int a = sides[0];
            int b = sides[1];
            int c = sides[2];

            Area = (float)Math.Sqrt((a + b + c) * (a + b - c) * (a - b + c) * (-a + b + c)) / 4f;

            return Area;
        }

        public float SetCircum()
        {
            return Circum = sides.Sum();

        }

        public bool Validate()
        {
            int a = sides[0];
            int b = sides[1];
            int c = sides[2];

            if (a + b > c && a + c > b && b + c > a)
                return true;
            return false;
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCircum();


        }
    }
}
