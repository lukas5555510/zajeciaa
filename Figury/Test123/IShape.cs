﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test123
{
    interface IShape
    {
        float GetArea();
        float GetCircum();
        IShape RandomizeShape(Random rand);
        IShape RandomizeShape();
        float SetArea();
        float SetCircum();
        bool Validate();
    }
}
