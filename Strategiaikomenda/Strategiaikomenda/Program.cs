﻿using System;
using Strategiaikomenda.Komenda;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategiaikomenda.Strategia;

namespace Strategiaikomenda
{
	class Program
	{
		static void Main(string[] args)
		{
			Krzesło krzesło = new Krzesło();
			Rura rura = new Rura();
			Świnia świnia = new Świnia();
			Astronauta astronauta = new Astronauta();

			List<IOsobowosc> lista = new List<IOsobowosc>();
			lista.Add(krzesło);
			lista.Add(rura);
			lista.Add(świnia);
			lista.Add(astronauta);


			foreach (var osob in lista)
			{
				osob.PodajNazwe();
			}

			IObliczanie dzialanie;

			float a = 60f;
			float b = 40f;

			//jakie działanie chce wykonać
			//1-dodawanie
			//2-odejmowanie
			//3-mnozenie
			//4-dzielenie

			int odp = 1;

			switch (odp)
			{
				case 1: dzialanie = new Suma(); break;
				case 2: dzialanie = new Odejmowanie(); break;
				case 3: dzialanie = new Mnożenie(); break;
				case 4: dzialanie = new Dzielenie(); break;
				default: dzialanie = new Suma();break;
			}

			Console.WriteLine(dzialanie.Oblicz(a, b));


			Console.ReadLine();
		}
	}
}
