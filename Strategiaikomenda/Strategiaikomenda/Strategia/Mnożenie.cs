﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategiaikomenda.Strategia
{
	public class Mnożenie : IObliczanie
	{
		public float Oblicz(float a, float b)
		{
			return a * b;
		}
	}
}
